# require_dependency 'boards_controller'
module BoardsControllerPatch
  def self.included(base)
    unloadable
    base.class_eval do
      alias_method_chain :index, :extended_boards
    end
  end

  def index_with_extended_boards
    @boards = @project.extended_boards.preload(:project, :last_message => :author).to_a
    # show the board if there is only one
    if @boards.size == 1
      @board = @boards.first
      show
    end
  end
end
BoardsController.send(:include, BoardsControllerPatch)
