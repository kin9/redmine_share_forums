require_dependency 'board'

module BoardShareBoardPatch
  def self.included(base)
    # base.extend(ClassMethods)
    base.send(:include, InstanceMethods)
    base.class_eval do
      attr_accessor :project_ids
      has_many :board_projects
      after_save :set_board_project
      safe_attributes 'name', 'description', 'parent_id', 'move_to', 'project_ids'
      # has_many :projects, through: :board_projects
      # belongs_to :project
    end
  end

  # module ClassMethods
  # end

  module InstanceMethods
    def set_board_project
      current_project_ids = board_projects.select(:id, :project_id)
      # delete
      delete_ids = current_project_ids.select{ |p| !project_ids.try(:include?, p.project_id.to_s) }.map(&:id)
      BoardProject.where(id: delete_ids).delete_all
      # insert
      project_ids.each do |project_id|
        unless current_project_ids.detect{ |p| p.project_id.to_s == project_id }
          BoardProject.create(board_id: id, project_id: project_id)
        end
      end if project_ids.present?
    end
  end
end
Board.send(:include, BoardShareBoardPatch)
