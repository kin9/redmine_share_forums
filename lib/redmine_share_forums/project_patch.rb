require_dependency 'project'

module BoardShareProjectPatch
  def self.included(base)
    # base.extend(ClassMethods)
    base.send(:include, InstanceMethods)
    base.class_eval do
      has_many :board_projects
      # has_many :boards, lambda { order("position ASC") }, :dependent => :destroy, :through => :board_projects
    end
  end

  module InstanceMethods
    def extended_boards
      board_ids = boards.pluck(:id) + board_projects.pluck(:board_id)
      Board.where(id: board_ids)
    end
  end
end
Project.send(:include, BoardShareProjectPatch)
