class BoardProject < ActiveRecord::Base
  unloadable
  belongs_to :project
  belongs_to :board
end
