Redmine::Plugin.register :redmine_share_forums do
  name 'Redmine Share Forums plugin'
  author 'Author name'
  description 'This is a plugin for Redmine'
  version '0.0.1'
  url 'http://example.com/path/to/plugin'
  author_url 'http://example.com/about'
  ActionDispatch::Callbacks.to_prepare do
    require 'redmine_share_forums/project_patch'
    require 'redmine_share_forums/board_patch'
    require 'redmine_share_forums/boards_controller_patch'
  end
end
