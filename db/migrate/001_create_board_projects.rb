class CreateBoardProjects < ActiveRecord::Migration
  def change
    create_table :board_projects do |t|
      t.integer :board_id
      t.integer :project_id
    end
    add_index :board_projects, [:board_id, :project_id], :unique => true
  end
end
